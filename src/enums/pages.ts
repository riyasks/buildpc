export enum Pages {
  processor = "processor",
  motherboard = "motherboard",
  ram = "ram",
  storage = "storage",
  graphicscard = "graphicscard",
  cabinet = "cabinet",
  powersupply = "powersupply",
  showall = "showall",
  home="home",
  test="test"
}