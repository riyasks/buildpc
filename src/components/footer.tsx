import React, { Fragment, useState, useContext, useEffect } from "react";

function Footer(): JSX.Element {
  return <Fragment>
      <footer className="border-top border-bottom">
  <div className="container">
     <p className="text-center">Disclaimer for affiliate links</p> 
    <ul className="nav justify-content-center">
      <li className="nav-item">
        <a className="nav-link" href="index.html">AssembleYourPC.Net</a>
      </li>
      <li className="nav-item">
        <a className="nav-link" href="rig.html">Build a PC</a>
      </li>
      <li className="nav-item">
        <a className="nav-link" href="gaming-laptops.html">Gaming Laptops</a>
      </li>
      <li className="nav-item">
        <a className="nav-link" href="about.html">About</a>
      </li>
      <li className="nav-item">
        <a className="nav-link" href="contact.html">Feedback</a>
      </li>
      <li className="nav-item">
        <a className="nav-link" href="privacy.html">Privacy</a>
      </li>
      <li className="nav-item">
        <a className="nav-link" href="contact.html">Contact</a>
      </li>
    </ul>
  </div>
</footer>

<section className="copyright-box">
  {/* <div className="container">
    <br>
    <p className="text-center text-muted">&copy; 2020 Random Experiments Web Solutions LLP</p>
  </div> */}
</section>
  </Fragment>;
}

export default Footer;
