import React, { Fragment, useState, useContext, useEffect } from "react";
import { ISTATE, Store } from "../Store";

function Processor(): JSX.Element {
  
    const { state, dispatch } = useContext(Store);
    const _data: ISTATE= state.data;

   
    return (
        <Fragment>Processor{JSON.stringify(_data.processors)}</Fragment>
    );
}

export default Processor;
