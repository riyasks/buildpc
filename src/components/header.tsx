import React, { Fragment, useState, useContext, useEffect } from "react";
import { Pages } from "../enums/pages";
import { useRoutes, useRedirect, A, navigate } from 'hookrouter';
function Header(): JSX.Element {
  // const [selectedComponent, setselectedComponent] = useState(Pages.processor)
  // const sett1 = ()=>{ return useRedirect('', '/');}

  return <Fragment>
    <nav className="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
      <div className="container">
        <a className="navbar-brand" href="index.html">
          <img alt="logo" height="30" src="https://assembleyourpc.net/assets/aypc_logo-cb631ba3ca795743b46a5aa4f5fedd8a24f97a98c15404254a164a15fb33edda.png" />
        </a>

        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTop"
          aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse justify-content-end" id="navbarTop">
          <ul className="navbar-nav">
            <li className="nav-item">
              <a className="nav-link" href="#">Recommended Rigs</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">Gaming PC</a>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" role="button"
                aria-haspopup="true" aria-expanded="false">{""}</a>
              <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a className="dropdown-item" onClick={() => {
                  navigate('/processors');
                }}>Processor</a>
                <a className="dropdown-item" onClick={() => {
                  navigate('/motherboards');
                }}>Motherboard</a>
                {/* // <a className="dropdown-item" onClick={(e) => { setselectedComponent(Pages.ram) }} >RAM</a>
                // <a className="dropdown-item" onClick={(e) => { setselectedComponent(Pages.storage) }} >Storage</a>
                // <a className="dropdown-item" onClick={(e) => { setselectedComponent(Pages.graphicscard) }} >Graphics Card</a>
                // <a className="dropdown-item" onClick={(e) => { setselectedComponent(Pages.cabinet) }} >Cabinet</a>
                // <a className="dropdown-item" onClick={(e) => { setselectedComponent(Pages.processor) }} >Power Supply</a>
                // <div className="dropdown-divider" onClick={(e) => { setselectedComponent(Pages.powersupply) }}></div>
                // <a className="dropdown-item" onClick={(e) => { setselectedComponent(Pages.showall) }} >Show All</a>
             */}
              </div>
            </li>

          </ul>


          {/* <A href="/about">About Page</A>
          <A href="/contact">Contacts Page</A> */}
          <form className="form-inline">
            <a className="btn btn-sm btn-dark" href="rig.html">PC Builder</a>
          </form>
        </div>
      </div>
    </nav>
  </Fragment>;
}

export default Header;
