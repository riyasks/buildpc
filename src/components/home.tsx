


import React, { Fragment, useState, useContext, useEffect } from "react";
// import { Pages } from "../enums/pages";

function Home(): JSX.Element {
   
    return <Fragment>
        <div className="content">
            <section className="content-header">
                <div className="jumbotron jumbotron-fluid">
                    <div className="container">
                        <h1 className="display-4 title">
                            Assemble PC Online  
                        </h1>
                        <p className="lead">
                            A simple PC builder tool for Indian users.
                            Select parts from the curated list of components,
                            to build your desktop computer in a few minutes.
                            Whether you're building a general purpose computer or a gaming rig
                            or a PC for photo/video editing, this little tool is going to save you some time and effort.
      </p>
                        <br></br>
                        <a href="/test" className="btn btn-primary btn-lg">
                            Build a PC
      </a>
                    </div>
                </div>
            </section>

            <section className="section-pages">
                <div className="container">
                    <div className="row">
                        <div className="col-sm-6">
                            <h2>Not sure where to begin ?</h2>
                            <p>Check out these newly created rigs by fellow users!</p>
                            <ul>
                                <li><a href="rig/14892.html"> Rig #14892</a> :  <i className="fa fa-inr"></i> 156358</li>
                                <li><a href="rig/14893.html"> Rig #14893</a> :  <i className="fa fa-inr"></i> 17479</li>
                                <li><a href="rig/14894.html"> Rig #14894</a> :  <i className="fa fa-inr"></i> 111735</li>
                                <li><a href="rig/14895.html"> Rig #14895</a> :  <i className="fa fa-inr"></i> 50650</li>
                                <li><a href="rig/14896.html"> Rig #14896</a> :  <i className="fa fa-inr"></i> 48484</li>
                            </ul>
                        </div>

                        <div className="col-sm-6">
                            <h2>Pre configured rigs</h2>
                            <p>You can easily customize these to suit your needs. </p>
                            <ul>
                                <li><a href="rig/514.html">Rig #514 (30k)</a> - Budget PC (General Purpose) - AMD </li>
                                <li><a href="rig/517.html">Rig #517 (34k)</a> - Budget PC (General Purpose) - Intel  </li>
                                <li><a href="rig/528.html">Rig #528 (52k)</a> - Gaming PC (Low Budget) - Ryzen 5 3400G  </li>
                                <li><a href="rig/524.html">Rig #524 (61k)</a> - Gaming PC (Budget) - RX 570  </li>
                                <li><a href="rig/525.html">Rig #525 (64k)</a> - Gaming PC (Budget) - GTX 1050  </li>
                            </ul>
                        </div>

                    </div>

                </div>
            </section>

        </div> 
    </Fragment>;
}

export default Home;
