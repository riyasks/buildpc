import React, { Fragment, useState, useContext, useEffect } from "react";
import { ISTATE, Store } from "../Store";

function Motherboard(): JSX.Element {

  const { state, dispatch } = useContext(Store);
  const _data: ISTATE = state.data;


  return (
    <Fragment>Motherboard{JSON.stringify(_data.motherboards)}</Fragment>
  );
}

export default Motherboard;
