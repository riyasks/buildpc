import React, { Fragment, useState, useContext, useEffect } from "react";
import { ISTATE, Store } from "../Store";

function Processor(): JSX.Element {

    const { state, dispatch } = useContext(Store);
    const _data: ISTATE = state.data;


    return (
        <Fragment>

            <div className="content">
                <section className="content-header">
                    <div className="jumbotron jumbotron-fluid mini-jumbo">
                        <div className="container">
                            <h1 className="title h2">
                                Select A Processor
      </h1>
                            <p className="lead text-muted">
                                (CPU)
      </p>
                        </div>
                    </div>
                </section>


                <section className="section-component-items">
                    <div className="container">
                        <table className="table table-responsive-sm">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Cores</th>
                                    <th>Speed</th>
                                    <th>Power</th>
                                    {/* <!-- <th>Rating</th> --> */}
                                    <th>Price</th>
                                </tr>
                            </thead>

                            <tbody id="tb-component-items">
                  

                                {
                                    _data.processors.map((data) =>
                                        <tr >
                                            <td className="td-name">
                                                <div className="row">
                                               
                                                    <div className="col-sm-2">
                                                        <picture className="component-img-sm">
                                                            <img src={"../"+data.image} />
                                                        </picture>
                                                    </div>
                                                    <div className="col-sm-10 name-wrapper">
                                                        <a target="_blank" rel="nofollow" href="https://amazon.in/dp/B07HJWVJDN?tag=aypc-21">AMD Athlon 200GE</a>
                                                    </div>
                                                </div>

                                            </td>
                                            <td>2</td>
                                            <td>3.2 GHz</td>
                                            <td>35 W</td>
                                            {/* <!-- <td>NA</td> --> */}
                                            <td>
                                                <div className="row">
                                                    <div className="col-sm-8 ">
                                                        <i className="fa fa-inr"></i> 4290.0
              </div>

                                                    <div className="col-sm-4 add-btn-column">
                                                        <a className="btn btn-sm btn-dark add-component-btn"
                                                        >
                                                            Add
               </a>
                                                    </div>

                                                </div>
                                            </td>

                                        </tr>
                                    )
                                }



                            </tbody>

                        </table>

                    </div>
                </section>

            </div>





        </Fragment>
    );
}

export default Processor;
