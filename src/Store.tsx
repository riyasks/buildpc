import React, { useReducer } from "react";
import { Cabinet, Cabinetfan, CpuCooler, GraphicsCard, Headphones, Keyboard, Monitor, Motherboard, Mouse, PowerSupply, Processor, RAM, Speakers } from "./models/computer";

export interface ISTATE {
  episodes: any[],
  favourites: any[],
  processors: Processor[],
  motherboards: Motherboard[],
  rams: RAM[],
  storages: Storage[],
  graphicscards: GraphicsCard[],
  cabinets: Cabinet[],
  powersupplys: PowerSupply[],

  cpuCoolers: CpuCooler[],
  cabinetfans: Cabinetfan[],
  monitors: Monitor[],
  keyboards: Keyboard[],
  mouses: Mouse[],
  headphones: Headphones[],
  speakers: Speakers[]
}
interface IACTION {
  type: any;
  payload: ISTATE;
}
const initialState: any = {};



export const Store = React.createContext<ISTATE | any>(initialState);


function reducer(state: ISTATE, action: IACTION) {
  switch (action.type) {
    case "FETCH_DATA":
      return { ...state, data: action.payload };


    default:
      return state

  }
}

export function StoreProvider(props: any): JSX.Element {
  const [state, dispatch] = useReducer(reducer, initialState)
  return (
    <Store.Provider value={{ state, dispatch }}>
      {props.children}
    </Store.Provider>
  );
}