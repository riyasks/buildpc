export interface Computer {
  id: number;
  processor: Processor;
  motherboard: Motherboard;
  ram: RAM;
  storage: Storage;
  graphicscard: GraphicsCard;
  cabinet: Cabinet;
  powersupply: PowerSupply;

  cpuCooler: CpuCooler;
  cabinetfan: Cabinetfan;
  monitor: Monitor;
  keyboard: Keyboard;
  mouse: Mouse;
  headphones: Headphones;
  speakers: Speakers;
}

export interface Processor {
  id: number;
  name: string;
  cores: number;
  speed: string;
  power: string;
  price: number;
  href: string;
  image: string;
  rating:string;
}
export interface Motherboard {
  id: number;
}
export interface RAM {
  id: number;
}
export interface Storage {
  id: number;
}
export interface GraphicsCard {
  id: number;
}
export interface Cabinet {
  id: number;
}
export interface PowerSupply  {
  id: number;
}
export interface CpuCooler {
  id: number;
}
export interface Cabinetfan {
  id: number;
}
export interface Monitor {
  id: number;
}
export interface Keyboard {
  id: number;
}
export interface Mouse {
  id: number;
}
export interface Headphones {
  id: number;
}
export interface Speakers {
  id: number;
}


// export interface Base {
//   id: number;
// }