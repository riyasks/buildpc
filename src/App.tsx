import { useRoutes } from 'hookrouter';
import React, { Fragment, useState,useContext, useEffect } from 'react';

import './App.css';
import Footer from './components/footer';
import Header from './components/header';
import Home from './components/home';
import Processor from './components/processors';
import { Pages } from './enums/pages';
import {ISTATE, Store} from "./Store";
import Motherboard from './components/motherboard';


const routes = {
  "/": () => <Home />,
  "/motherboards": () => <Motherboard />,
   "/processors": () => <Processor />,
  // "/contact": () => <Contact />
};

function App():JSX.Element {
const [value, setvalue] = useState<boolean>(false)
// const [Todo, setTodo] = useState<ITODO[]>([])

// const Add = () => {
//   const newTod:ITODO[]=[...Todo,{text:value,complete:false}]
//   setTodo(newTod);
//    setvalue('');
// };
const {state,dispatch} = useContext(Store)
  const routeResult = useRoutes(routes);

  
useEffect(() => {
  
 fetchAction()

console.log(state);
  return () => {
    
  }
}, [])

const fetchAction=async ()=>{
  // const url =
  //  // "http://api.tvmaze.com/singlesearch/shows?q=rick-&-morty&embed=episodes";
  //  "./models/processor.json"
  // const data= await fetch(url);
  // const datajson=await data.json();


  var cabinet = require('./models/cabinet.json');//14
  var cabinetfan = require('./models/cabinetfan.json');
  var cpuCooler = require('./models/cpuCooler.json');
  var graphicscard = require('./models/graphicscard.json');
  var headphones = require('./models/headphones.json');
  var keyboard = require('./models/keyboard.json');
  var monitor = require('./models/monitor.json');
  var motherboard = require('./models/motherboard.json');
  var mouse = require('./models/mouse.json');
  var powersupply = require('./models/powersupply.json');
  var processor = require('./models/processor.json');
  var ram = require('./models/ram.json');
  var speakers = require('./models/speakers.json');
  var storage = require('./models/storage.json');

  var payload:ISTATE={
    cabinetfans:cabinetfan,
    cabinets:cabinet,
    cpuCoolers:cpuCooler,
    episodes:[],
    favourites:[],
    graphicscards:graphicscard,
    headphones:headphones,
    keyboards:keyboard,
    monitors:monitor,
    motherboards:motherboard,
    mouses:mouse,
    powersupplys:powersupply,
    processors:processor,
    rams:ram,
    speakers:speakers,
    storages:storage
  
  }



setvalue(true);
 
  return dispatch({
    type: "FETCH_DATA",
    payload: payload
  });

  
}

  return (
    <Fragment>
      <Header />
      {value? routeResult:""}
      <Footer />

    </Fragment>
  );
}

export default App;
